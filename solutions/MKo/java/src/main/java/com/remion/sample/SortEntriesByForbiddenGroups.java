package com.remion.sample;

import java.util.Comparator;

public class SortEntriesByForbiddenGroups implements Comparator<Entry> {

	@Override
	public int compare(Entry o1, Entry o2) {
		return Integer.compare(o1.getForbiddenGroups().size(),o2.getForbiddenGroups().size());
	}

}
