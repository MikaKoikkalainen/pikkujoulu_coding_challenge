package com.remion.sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Result {
	private List<Group> groups;
	private Map<Group,List<Entry>> result;
	
	public Result(List<Group> groups) {
		this.groups = groups;
		this.result = new TreeMap<Group, List<Entry>>();
		for (Group group : groups) {
			this.result.put(group, new ArrayList<Entry>());
		}
	}
	
	public void addEntry(Group group, Entry entry) {
		result.get(group).add(entry);
	}
	
	public void removeEntry(Group group, Entry entry) {
		result.get(group).remove(entry);
	}
	
	public int getEntryCount(Group group) {
		return result.get(group).size();
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	
	public List<Entry> getEntriesForGroup(Group group) {
		List<Entry> entries = result.get(group);
		Collections.sort(entries);
		return entries;
	}

	public int resultIdealness(SolutionInfo info) {
		int incompleteGroupCount = info.getIncompleteGroupCountWithoutRestrictions();
		int idealness = 0;
		for (Group group : groups) {
			int entryCountForGroup = result.get(group).size();
			if (info.getMaxEntryCountForGroup(group) < info.getIdealEntryCountPerGroup()) {
				if (info.getMaxEntryCountForGroup(group) == entryCountForGroup) {
					// Group cannot have any more entries so its count is considered to be ideal
				} else {
					idealness += Math.abs(info.getMaxEntryCountForGroup(group) - entryCountForGroup);
				}
			} else if (info.getMinEntryCountForGroup(group) > info.getIdealEntryCountPerGroup()) {
				if (info.getMinEntryCountForGroup(group) == entryCountForGroup) {
					// Group has minimum of X entries and this is considered to be ideal count for that group
				} else {
					idealness += Math.abs(info.getMinEntryCountForGroup(group) - entryCountForGroup);
				}
			} else if (incompleteGroupCount > 0 && (info.getIdealEntryCountPerGroupWithoutRestrictions() - 1) == entryCountForGroup) {
				incompleteGroupCount--;
			} else {
				idealness += Math.abs(info.getIdealEntryCountPerGroupWithoutRestrictions() - entryCountForGroup);
			}
		}
		return idealness;
	}
	
	public int getIncompleteGroupCountWithoutRestrictions(SolutionInfo info) {
		int idealCount = info.getIdealEntryCountPerGroupWithoutRestrictions();
		
		int incompleteGroupCount = 0;
		
		for (Group group : groups) {
			int entryCountForGroup = result.get(group).size();
			if (info.getMaxEntryCountForGroup(group) < info.getIdealEntryCountPerGroup() && info.getMaxEntryCountForGroup(group) == entryCountForGroup) {
				// Group cannot have any more entries so its count is considered to be ideal
			} else if (info.getMinEntryCountForGroup(group) > info.getIdealEntryCountPerGroup() && info.getMinEntryCountForGroup(group) == entryCountForGroup) {
				// Group has minimum of X entries and this is considered to be ideal count for that group
			} else if (idealCount < entryCountForGroup) {
				incompleteGroupCount++;
			}
		}
		
		return incompleteGroupCount;
	}
	
	public void printEntryCountsPerGroup() {
		System.out.println();
		for (Group group : groups) {
			System.out.println(group.getName() + " entry count=" + getEntryCount(group));
		}
		System.out.println();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groups == null) ? 0 : groups.hashCode());
		result = prime * result
				+ ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		if (groups == null) {
			if (other.groups != null)
				return false;
		} else if (!groups.equals(other.groups))
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		return true;
	}
}
