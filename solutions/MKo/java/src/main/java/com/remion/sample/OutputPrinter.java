package com.remion.sample;

import java.util.List;

public class OutputPrinter {

	public void printResult(Result result) {
		System.out.println();
		List<Group> groups = result.getGroups();
		for (Group group : groups) {
			List<Entry> entriesForGroup = result.getEntriesForGroup(group);
			StringBuffer sb = new StringBuffer();
			sb.append(group.getName());
			sb.append(": ");
			for (int i = 0; i < entriesForGroup.size(); ++i) {
				if (i != 0) {
					sb.append(", ");
				}
				sb.append(entriesForGroup.get(i).getName());
			}
			System.out.println(sb.toString());
		}
		

		System.out.println();		
	}
}
