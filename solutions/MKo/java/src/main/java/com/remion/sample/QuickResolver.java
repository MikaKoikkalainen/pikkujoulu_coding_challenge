package com.remion.sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickResolver {
	private SolutionInfo info;
	private List<Entry> entries;
	private List<Group> groups;
	
	public QuickResolver(SolutionInfo info) {
		this.info = info;
		this.entries = info.getEntries();
		this.groups = info.getGroups();
	}

	public Result findResult(boolean allowNonIdealResult) {
		Result result = new Result(groups);
		
		Collections.sort(entries, new SortEntriesByForbiddenGroups());
		Collections.reverse(entries);
		
		List<Entry> secondPass = new ArrayList<Entry>();
		
		for (Entry entry : entries) {
			Group groupToAdd = findSuitableGroupWithLeastEntries(entry,result,info);
			
			if (groupToAdd == null) {
				secondPass.add(entry);
			} else {
				result.addEntry(groupToAdd, entry);
			}
		}
		
		List<Entry> thirdPass = new ArrayList<Entry>();
		
		for (Entry entry : secondPass) {
			// Try to find another entry which could be moved from already assigned group
			boolean entrySwapped = swapEntryInResult(entry,result,info);
			
			if (!entrySwapped) {
				thirdPass.add(entry);
			} 
		}
		
		// Just add entry to group which has least number of entries... this probably is not ideal...
		for (Entry entry : thirdPass) {
			Group groupToAdd = null;
			int minEntries = Integer.MAX_VALUE;
			for (Group group : entry.getPossibleGroups()) {
				int entryCount = result.getEntryCount(group);
				if (minEntries > entryCount) {
					groupToAdd = group;
					minEntries = entryCount;
				}
			}
			
			if (groupToAdd == null) {
				// This should not happen
				if (Config.IS_DEBUG_ENABLED) {
					System.out.println("Failed to find result, entry=" + entry.getName());
				}
				return null;
			} else {
				result.addEntry(groupToAdd, entry);
			}			
		}
		
		int idealness = result.resultIdealness(info);
		if (Config.IS_DEBUG_ENABLED) {
			System.out.println("Quick result idealness is " + idealness);
		}
		if (idealness == 0) {
			return result;
		} else {
			if (allowNonIdealResult) {
				return result;
			} else {
				return null;
			}
		}		
	}
	
	public Group findSuitableGroupWithLeastEntries(Entry entry, Result result, SolutionInfo info) {
		Group groupToAdd = null;
		int minEntries = Integer.MAX_VALUE;
		int maxEntryCountForGroup = 0;
		
		int idealEntryCount = info.getIdealEntryCountPerGroupWithoutRestrictions();
		int maxIncompleteGroupCount = info.getIncompleteGroupCountWithoutRestrictions();
		int incompleteGroupCount = result.getIncompleteGroupCountWithoutRestrictions(info);
		if (maxIncompleteGroupCount >= incompleteGroupCount) {
			idealEntryCount--;
		}
		
		for (Group group : entry.getPossibleGroups()) {
			int entryCount = result.getEntryCount(group);
			if (entryCount < idealEntryCount || 
				entryCount < info.getMinEntryCountForGroup(group)) {
				if (minEntries > entryCount || (minEntries == entryCount && info.getMaxEntryCountForGroup(group) > maxEntryCountForGroup)) {
					groupToAdd = group;
					minEntries = entryCount;
					maxEntryCountForGroup = info.getMaxEntryCountForGroup(group);
				}
			}
		}
		
		return groupToAdd;
	}
	
	public boolean swapEntryInResult(Entry entry, Result result, SolutionInfo info) {
		boolean entrySwapped = false;
		
		for (Group group : entry.getPossibleGroups()) {
			List<Entry> resultEntries = result.getEntriesForGroup(group);
			for (Entry resultEntry : resultEntries) {
				for (Group resultGroup : resultEntry.getPossibleGroups()) {
					int resultEntryCount = result.getEntryCount(resultGroup);
					if (resultEntryCount < (info.getIdealEntryCountPerGroupWithoutRestrictions() - 1) || 
						resultEntryCount < info.getMinEntryCountForGroup(group)) {

						// We found entry which we could swap
						result.removeEntry(group, resultEntry);
						result.addEntry(group, entry);
						result.addEntry(resultGroup, resultEntry);
						entrySwapped = true;
						break;
					}
				}
				if (entrySwapped) {
					break;
				}
			}
			if (entrySwapped) {
				break;
			}
		}

		if (!entrySwapped) {
			for (Group group : entry.getPossibleGroups()) {
				List<Entry> resultEntries = result.getEntriesForGroup(group);
				for (Entry resultEntry : resultEntries) {
					for (Group resultGroup : resultEntry.getPossibleGroups()) {
						int resultEntryCount = result.getEntryCount(resultGroup);
						if (resultEntryCount < info.getIdealEntryCountPerGroupWithoutRestrictions() || 
							resultEntryCount < info.getMinEntryCountForGroup(group)) {
							
							// We found entry which we could swap
							result.removeEntry(group, resultEntry);
							result.addEntry(group, entry);
							result.addEntry(resultGroup, resultEntry);
							entrySwapped = true;
							break;
						}
					}
					if (entrySwapped) {
						break;
					}
				}
				if (entrySwapped) {
					break;
				}
			}
		}
		
		return entrySwapped;
	}
	
}
