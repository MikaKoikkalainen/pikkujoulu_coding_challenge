package com.remion.sample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Resolver {

	private SolutionInfo info;
	
	
	public Resolver(SolutionInfo info) {
		this.info = info;
	}
	
	// This is where the magic happens
	public Result resolve() {
		Result res = null;
		if (info.getEntries() != null && info.getGroups().size() > 0) {
			if (Config.IS_DEBUG_ENABLED) {
				printStatistics();
			}
			
			if (info.getUsedAlgorithm() == Config.ALGORITHM_QUICK) {
				QuickResolver qr = new QuickResolver(info);
				res = qr.findResult(true);
			} else if (info.getUsedAlgorithm() == Config.ALGORITHM_BRUTE_FORCE) {
				BruteForceResolver bfr = new BruteForceResolver(info);
				res = bfr.findResult();
			} else if (info.getUsedAlgorithm() == Config.ALGORITHM_IDEAL) {
				QuickResolver qr = new QuickResolver(info);
				res = qr.findResult(false);
				if (res == null) {
					BruteForceResolver bfr = new BruteForceResolver(info);
					res = bfr.findResult();					
				}
			}
			
			
		} else {
			res = new Result(info.getGroups());
		}
		
		
		if (Config.IS_DEBUG_ENABLED && res != null) {
			res.printEntryCountsPerGroup();
		}
		
		return res;
	}

	public void printStatistics() {
		System.out.println();
		System.out.println("Entries=" + info.getEntries().size() + ", Groups=" + info.getGroups().size());
		System.out.println("IdealEntryCountPerGroup=" + info.getIdealEntryCountPerGroup());
		System.out.println("IncompleteGroupCount=" + info.getIncompleteGroupCount());
		System.out.println("EntriesWithoutRestictions=" + info.getEntriesWithoutRestrictions() + ", GroupsWithoutRestrictions=" + info.getGroupsWithoutRestrictions());
		System.out.println("IdealEntryCountPerGroupWithoutRestrictions=" + info.getIdealEntryCountPerGroupWithoutRestrictions());
		System.out.println("IncompleteGroupCountWithoutRestrictions=" + info.getIncompleteGroupCountWithoutRestrictions());				
		System.out.println("PossibleResultCount=" + info.getPossibleResultCount());
		System.out.println("UsedAlgorithm=" + Config.algorithmToString(info.getUsedAlgorithm()));
		
		for (Group group : info.getGroups()) {
			System.out.println(group.getName() + ", min=" + info.getMinEntryCountForGroup(group) + ", max=" + info.getMaxEntryCountForGroup(group));
		}
		
		System.out.println();
	}

}
