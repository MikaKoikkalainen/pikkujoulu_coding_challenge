package com.remion.sample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * 
 */
public class PJCCMain {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
    	
    	short usedAlgorithm = Config.ALGORITHM_QUICK;
    	for (String arg : args) {
    		if ("--help".equals(arg) || "-h".equals(arg)) {
    			printHelp();
    			System.exit(0);
    		} else if ("--verbose".equals(arg)) {
    			Config.IS_DEBUG_ENABLED = true;
    		} else if ("--brute-force".equals(arg)) {
    			usedAlgorithm = Config.ALGORITHM_BRUTE_FORCE;
    		} else if ("--quick".equals(arg)) {
    			usedAlgorithm = Config.ALGORITHM_QUICK;
    		} else if ("--ideal".equals(arg)) {
    			usedAlgorithm = Config.ALGORITHM_IDEAL;
    		} else if (arg.startsWith("--generate=")) {
    			InputGenerator ig = new InputGenerator();
    			String[] params = arg.substring("--generate=".length()).split(",");
    			if (params.length == 3) {
    				BufferedWriter bw = null;
    				try {
    					bw = new BufferedWriter(new PrintWriter("input.txt", "UTF-8"));
    					ig.generate(bw, Integer.parseInt(params[0]), Integer.parseInt(params[1]), Integer.parseInt(params[2]));
    				} finally {
    					if (bw != null) {
    						try {
								bw.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
    					}
    				}
    				System.exit(0);
    			} else {
    				System.err.println("Invalid params for --generate: " + arg);
    				System.exit(1);
    			}
    		}
    	}
    	
    	
    	InputParser ip = new InputParser();
    	boolean readSuccessful = false;
    	BufferedReader br = null;
    	try {
    		br = new BufferedReader(new InputStreamReader(System.in));
    		readSuccessful = ip.readInput(br);
    	} finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }    		
    	}
    		
    	if (readSuccessful) {
    		SolutionInfo info = new SolutionInfo(ip.getEntries(),ip.getGroups(),usedAlgorithm);
    		Resolver resolver = new Resolver(info);
    		Result res = resolver.resolve();
    		
    		OutputPrinter op = new OutputPrinter();
    		op.printResult(res);
    		System.exit(0);
    	} else {
    		System.err.println("Input parsing failed!!!");
    		System.exit(-1);
    	}
    }
    
    private static void printHelp() {
    	System.out.println();
    	System.out.println("Mika's Resolver v1.0");
    	System.out.println("====================");
    	System.out.println("Command line options:");
    	System.out.println(" --help, -h           This help");
    	System.out.println(" --verbose            Enable debug logging");
    	System.out.println(" --brute-force        Use brute force algorithm for resolving");
    	System.out.println(" --quick              Use quick algorithm for resolving. This is default.");
    	System.out.println(" --ideal              Try first resolving using quick algorithm and if it does not find ideal solution, use brute force");
    	System.out.println();
    }
}
