package com.remion.sample;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.Random;

public class InputGenerator {

	public InputGenerator() {
	}
	
	public void generate(BufferedWriter bw, int groupCount, int entryCount, int rand) {

		try{
		    StringBuffer sb = new StringBuffer();
		    for (int i = 0; i < groupCount; ++i) {
		    	if (i != 0) {
		    		sb.append(",");
		    	}
		    	sb.append("Group " + (i+1));
		    }
		    bw.write(sb.toString() + "\n");
		    
			Random r = new Random();
		    for (int j = 0; j < entryCount; ++j) {
		    	StringBuffer sb2 = new StringBuffer();
		    	sb2.append("Name" + (j+1));
		    	sb2.append(getRandomGroups(groupCount, r, rand));
		    	bw.write(sb2.toString() + "\n");
		    }
		} catch (Exception e) {
		   e.printStackTrace();
		}
	}
	
	public String getRandomGroups(int groupCount, Random r, int rand) {
		StringBuffer sb = new StringBuffer();
		int allowedGroup = r.nextInt(groupCount);
		
		for (int i = 0; i < groupCount; ++i) {
			if (allowedGroup != i) {
				for (int j = 0; j < rand; ++j) {
					if (r.nextBoolean()) {
						sb.append(",");
						sb.append((i+1));
						break;
					}
				}				
			}
		}
		return sb.toString();
	}
}
