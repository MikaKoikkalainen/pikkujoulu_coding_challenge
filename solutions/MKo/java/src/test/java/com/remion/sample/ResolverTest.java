package com.remion.sample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class ResolverTest {
	
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                 { 3,10,0,Config.ALGORITHM_QUICK }, 
                 { 3,0,0,Config.ALGORITHM_QUICK }, 
                 { 5,4,0,Config.ALGORITHM_QUICK }, 
                 { 5,1,0,Config.ALGORITHM_QUICK }, 
                 { 15,5000,0,Config.ALGORITHM_QUICK }/*, 
                 { 15,500,1,Config.ALGORITHM_QUICK }, 
                 { 15,500,2,Config.ALGORITHM_QUICK }, 
                 { 15,500,3,Config.ALGORITHM_QUICK }, 
                 { 15,500,4,Config.ALGORITHM_QUICK }, 
                 { 15,500,5,Config.ALGORITHM_QUICK }, 
                 { 15,500,6,Config.ALGORITHM_QUICK } */
           });
    }

    @Parameter(value = 0)
    public /* NOT private */ int fGroupCount;

    @Parameter(value = 1)
    public /* NOT private */ int fEntryCount;	
	
    @Parameter(value = 2)
    public /* NOT private */ int fRand;
    
    @Parameter(value = 3)
    public /* NOT private */ short fUsedAlgorithm;    
    
    @Test
    public void testResolver() {
    	runTestWithParams(fGroupCount,fEntryCount,fRand,fUsedAlgorithm);
    }
 
	private void runTestWithParams(int groupCount, int entryCount, int rand, short usedAlgorithm) {
		String input = generateInputAsString(groupCount, entryCount, rand);
		if (input == null) {
			fail();
		}
		SolutionInfo info = parseInput(input,usedAlgorithm);
		if (info != null) {
			Resolver resolver = new Resolver(info);
    		Result res = resolver.resolve();
    		if (res.resultIdealness(info) != 0) {
    			resolver.printStatistics();
    			System.out.println("Result idealness is " + res.resultIdealness(info));
    			res.printEntryCountsPerGroup();
        		OutputPrinter op = new OutputPrinter();
        		op.printResult(res);
    			fail();
    		}
    	} else {
    		fail();
    	}
	}
	
	private SolutionInfo parseInput(String input, short usedAlgorithm) {
    	InputParser ip = new InputParser();
    	boolean readSuccessful = false;
    	BufferedReader br = null;
    	try {
    		br = new BufferedReader(new StringReader(input));
    		readSuccessful = ip.readInput(br);
    	} finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }    		
    	}
    	
    	if (readSuccessful) {
    		return new SolutionInfo(ip.getEntries(),ip.getGroups(),usedAlgorithm);
    	} else {
    		return null;
    	}
	}
	
	private String generateInputAsString(int groupCount, int entryCount, int rand) {
		InputGenerator ig = new InputGenerator();
		BufferedWriter bw = null;
		StringWriter sw = null;

		String result = null;
		
		try {
			sw = new StringWriter();
			bw = new BufferedWriter(sw);
			ig.generate(bw, groupCount, entryCount, rand);
			bw.flush();
			result = sw.getBuffer().toString();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (sw != null) {
				try {
					sw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
			
		return result;
	}
}
